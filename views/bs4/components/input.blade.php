<div class="form-group">
    <x-formbuilder::common.label :data="$data" />
    <input {{ $attributes->only([
        'name',
        'id',
        'placeholder',
        'required',
        'value',
        'type',
        'list'
    ])->merge([
        'class' => 'form-control',
    ]) }} {{$attributes->whereStartsWith('aria')}} />
    <x-formbuilder::common.data-list :data="$data" />
    <x-formbuilder::common.help-text :data="$data" />
    <x-formbuilder::common.errors :data="$data" />
</div>