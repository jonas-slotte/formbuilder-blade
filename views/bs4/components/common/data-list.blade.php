@if($enabled)
<datalist {{$attributes->merge([
    'id' => $id
])}}>
    @foreach($options as $value)
    <option value="{{$value}}" />
    @endforeach
</datalist>
@endif