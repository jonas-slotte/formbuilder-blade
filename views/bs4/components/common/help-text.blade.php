@if($enabled)
<small {{ $attributes->merge([
        'id' => $ariaDescribedById,
        'class' => 'form-text text-muted'
    ]) }}>
    {{$content}}
</small>
@endif