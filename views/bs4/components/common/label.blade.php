<label {{$attributes->merge([
    'for' =>$id,
    'class' => $hidden ? "sr-only" : ''
])}}>
    {{$label}}
    @if($required === true)
    @include('formbuilder::components.common.required')
    @elseif($optional === true)
    @include('formbuilder::components.common.optional')
    @endif
</label>