<div class="form-group">
    <x-formbuilder::common.label :data="$data" />
    <textarea {{ $attributes->only([
        'name',
        'id',
        'placeholder',
        'required'
    ])->merge([
        'class' => 'form-control',
    ]) }} {{$attributes->whereStartsWith('aria')}}>{{$attributes->get('value')}}</textarea>
    <x-formbuilder::common.help-text :data="$data" />
    <x-formbuilder::common.errors :data="$data" />
</div>