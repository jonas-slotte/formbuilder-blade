<div class="form-group">
    <x-formbuilder::common.label :data="$data" />
    <select {{ $attributes->only([
        'name',
        'id',
        'required',
        'multiple',
        'size'
    ])->merge([
        'class' => 'custom-select',
    ]) }} {{$attributes->whereStartsWith('aria')}} class="custom-select">
        @foreach($options as $value => $text)
        <option value="{{$value}}" {{ $isSelected($value) ? 'selected=selected' : '' }}
            {{ $isDisabled($value) ? 'disabled=disabled' : '' }}>{{$text}}</option>
        @endforeach
    </select>

    <x-formbuilder::common.help-text :data="$data" />
    <x-formbuilder::common.errors :data="$data" />
</div>