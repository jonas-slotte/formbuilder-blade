<?php

return [
    /**
     * The default theme to use when not explicitly prefixing.
     * Valid values: (any key specified in "themes")
     * 
     * This will allow the theme to be runtime changed with config(['formbuilder-blade.theme'=>'whatevs'])
     * Unlikely that the same page will have differently themed forms, but that could work with @php calls?
     */
    'theme' => 'bs4',
    /**
     * Themes that can will be provided by this package.
     * These may be called explicitly using the formbuilder-{theme}:: syntax.
     * Array value is: TBD
     */
    'themes' => [
        'bs4' => []
    ]
];
