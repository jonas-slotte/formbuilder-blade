<?php

namespace JonasSlotte\FormBuilderBlade\View\Components;

use Illuminate\View\Component;
use Illuminate\View\ComponentAttributeBag;

class Select extends Optionable
{
    public function render()
    {
        return function ($data) {
            return view('formbuilder::components.select', $this->getData($data["attributes"]))->render();
        };
    }
}
