<?php

namespace JonasSlotte\FormBuilderBlade\View\Components;

use Illuminate\View\Component;
use Illuminate\View\ComponentAttributeBag;

class Optionable extends BaseComponent
{
    public function __construct($data = null, $options = null, $selected = null)
    {
        parent::__construct($data);
        if ($options) {
            $this->data->offsetSet('options', $options);
        }
        if ($selected) {
            $this->data->offsetSet('selected', $selected);
        }
    }

    protected function getMultiple(ComponentAttributeBag $attributes)
    {
        return $attributes->get('multiple', null);
    }

    protected function getSize(ComponentAttributeBag $attributes)
    {
        return $attributes->get('size', null);
    }

    protected function getSelected(ComponentAttributeBag $attributes)
    {
        return $attributes->get('selected', []);
    }
    protected function getOptions(ComponentAttributeBag $attributes)
    {
        return $attributes->get('options', []);
    }

    /**
     * Is the given value selected
     *
     * @param mixed $value
     * @return boolean
     */
    public function isSelected($value)
    {
        $selected = $this->getSelected($this->getInitialData());
        $selectedCount = sizeof($selected);
        $placeholderValue = $value == null;
        if ($selectedCount === 0 && $placeholderValue) {
            return true;
        }
        return in_array($value, $selected);
    }

    /**
     * Is the given value disabled
     *
     * @param mixed $value
     * @return boolean
     */
    public function isDisabled($value)
    {
        $required = $this->getRequired($this->attributes);
        $selected = $this->getSelected($this->attributes);
        $placeholderValue = $value == null;
        if ($placeholderValue && $required) {
            return true;
        }
        return false;
    }


    protected function getData(ComponentAttributeBag $attributes)
    {
        $attributes = $this->getInitialData()->merge($attributes->getAttributes());

        $id = $this->getId($attributes);
        $value = $this->getValue($attributes);
        $helpText = $this->getHelpText($attributes);
        $multiple = $this->getMultiple($attributes);
        $size = $this->getSize($attributes);
        $label = $this->getLabel($attributes);
        $hideLabel = $this->getHideLabel($attributes);
        $placeholder = $this->getPlaceholder($attributes);
        $options = $this->getOptions($attributes);

        $set = [
            'id' => $id,
            'name' =>  $this->getName($attributes),
            'required' => $this->getRequired($attributes),
            'value' => $value,
            'label' => $label,
            'hide-label' => $hideLabel
        ];

        if ($multiple) {
            $set['multiple'] = true;
        }

        if ($size !== null) {
            $set['size'] = $size;
        }

        if ($helpText !== null) {
            $describedById = uuidString();
            $set['aria-describedby'] = $describedById;
            $set['help-text'] = $helpText;
        }

        $this->withAttributes($set);

        $data = $set;

        if ($placeholder !== null) {
            $options = array_merge([null => $placeholder], $options);
        }


        return [
            'attributes' => $this->attributes,
            'componentName' => $this->componentName,
            'data' => $data,
            'options' => $options,
            'isSelected' => function ($value) {
                return $this->isSelected($value);
            },
            'isDisabled' => function ($value) {
                return $this->isDisabled($value);
            }
        ];
    }

    public function render()
    {
        return function ($data) {
            return view('formbuilder::components.input', $this->getData($data["attributes"]))->render();
        };
    }
}
