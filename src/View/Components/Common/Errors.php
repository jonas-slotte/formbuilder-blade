<?php

namespace JonasSlotte\FormBuilderBlade\View\Components\Common;

use Illuminate\View\Component;
use Illuminate\View\ComponentAttributeBag;
use JonasSlotte\FormBuilderBlade\View\Components\BaseComponent;

class Errors extends BaseComponent
{
    public function render()
    {
        return view('formbuilder::components.common.errors', [
            'key' => $this->getInitialData()->first('name')
        ]);
    }
}
