<?php

namespace JonasSlotte\FormBuilderBlade\View\Components\Common;

use Illuminate\View\Component;
use Illuminate\View\ComponentAttributeBag;
use JonasSlotte\FormBuilderBlade\View\Components\BaseComponent;

class DataList extends BaseComponent
{
    public function render()
    {
        $dataList = $this->getInitialData()->get('data-list');
        $enabled = sizeof($dataList) > 0;
        if ($enabled) {
            $options = $dataList;
            $id =  $this->getInitialData()->get('list');
        } else {
            $id = "undefined";
            $options = [];
        }
        return view('formbuilder::components.common.data-list', [
            'options' => $options,
            'id' => $id,
            'enabled' => $enabled
        ]);
    }
}
