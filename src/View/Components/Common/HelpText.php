<?php

namespace JonasSlotte\FormBuilderBlade\View\Components\Common;

use Illuminate\View\Component;
use Illuminate\View\ComponentAttributeBag;
use JonasSlotte\FormBuilderBlade\View\Components\BaseComponent;

class HelpText extends BaseComponent
{
    public function render()
    {
        $helpText = $this->getInitialData()->get('help-text');
        if ($helpText) {
            $content = $helpText;
            $id =  $this->getInitialData()->get('aria-describedby');
        } else {
            $id = "undefined";
            $content = null;
        }
        return view('formbuilder::components.common.help-text', [
            'ariaDescribedById' => $id,
            'content' => $content,
            'enabled' => $helpText !== null
        ]);
    }
}
