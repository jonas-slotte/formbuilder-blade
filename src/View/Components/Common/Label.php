<?php

namespace JonasSlotte\FormBuilderBlade\View\Components\Common;

use Illuminate\View\Component;
use Illuminate\View\ComponentAttributeBag;
use JonasSlotte\FormBuilderBlade\View\Components\BaseComponent;

class Label extends BaseComponent
{
    public function render()
    {

        return view('formbuilder::components.common.label', [
            'id' => $this->getInitialData()->get('id'),
            'label' => $this->getInitialData()->get('label'),
            'required' => $this->getInitialData()->get('required'),
            'optional' => $this->getInitialData()->get('optional'),
            'hidden' => $this->getInitialData()->get('hide-label', false),
        ]);
    }
}
