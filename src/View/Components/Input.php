<?php

namespace JonasSlotte\FormBuilderBlade\View\Components;

use Illuminate\View\Component;
use Illuminate\View\ComponentAttributeBag;

class Input extends BaseComponent
{
    protected function getInputType(ComponentAttributeBag $attributes)
    {
        return $attributes->get('type', 'text');
    }

    protected function getDataList(ComponentAttributeBag $attributes)
    {
        return $attributes->get('data-list', []);
    }


    protected function getData(ComponentAttributeBag $attributes)
    {
        $attributes = $this->getInitialData()->merge($attributes->getAttributes());

        $id = $this->getId($attributes);
        $value = $this->getValue($attributes);
        $helpText = $this->getHelpText($attributes);
        $type = $this->getInputType($attributes);
        $label = $this->getLabel($attributes);
        $hideLabel = $this->getHideLabel($attributes);
        $placeholder = $this->getPlaceholder($attributes);
        $dataList = $this->getDataList($attributes);
        $set = [
            'id' => $id,
            'type' =>  $type,
            'name' =>  $this->getName($attributes),
            'required' => $this->getRequired($attributes),
            'value' => $value,
            'label' => $label,
            'hide-label' => $hideLabel,
            'placeholder' => $placeholder
        ];

        if ($helpText !== null) {
            $describedById = uuidString();
            $set['aria-describedby'] = $describedById;
            $set['help-text'] = $helpText;
        }

        if ($dataList !== null) {
            $dataListId = uuidString();
            $set['list'] = $dataListId;
            $set['data-list'] = $dataList;
        }

        $this->withAttributes($set);

        return [
            'attributes' => $this->attributes,
            'componentName' => $this->componentName,
            'data' => $set
        ];
    }

    public function render()
    {
        return function ($data) {
            return view('formbuilder::components.input', $this->getData($data["attributes"]))->render();
        };
    }
}
