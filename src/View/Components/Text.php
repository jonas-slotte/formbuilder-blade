<?php

namespace JonasSlotte\FormBuilderBlade\View\Components;

use Illuminate\View\Component;
use Illuminate\View\ComponentAttributeBag;

class Text extends Input
{
    protected function getInputType(ComponentAttributeBag $attributes)
    {
        return 'text';
    }
}
