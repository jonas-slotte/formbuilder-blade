<?php

namespace JonasSlotte\FormBuilderBlade\View\Components;

use Illuminate\View\Component;
use Illuminate\View\ComponentAttributeBag;

class Textarea extends Input
{
    public function render()
    {
        return function ($data) {
            return view('formbuilder::components.textarea', $this->getData($data["attributes"]))->render();
        };
    }
}
