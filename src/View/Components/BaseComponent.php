<?php

namespace JonasSlotte\FormBuilderBlade\View\Components;

use Illuminate\Support\Collection;
use Illuminate\View\Component;
use Illuminate\View\ComponentAttributeBag;

/**
 * Allows setting attributes via array
 */
abstract class BaseComponent extends Component
{
    /** @var ComponentAttributeBag */
    protected $data;

    public function __construct($data = null)
    {
        if ($data instanceof ComponentAttributeBag) {
            $this->data = $data;
        } else {
            $this->data = new ComponentAttributeBag($data ?? []);
        }
    }
    public function getInitialData()
    {
        return $this->data;
    }
    protected function getValue(ComponentAttributeBag $attributes)
    {
        return $attributes->get('value', "");
    }
    protected function getHelpText(ComponentAttributeBag $attributes)
    {
        return $attributes->get('help-text', null);
    }
    protected function getId(ComponentAttributeBag $attributes)
    {
        return $attributes->get('id', uuidString());
    }
    protected function getRequired(ComponentAttributeBag $attributes)
    {
        return $attributes->get('required', false);
    }
    protected function getOptional(ComponentAttributeBag $attributes)
    {
        return $attributes->get('optional', false);
    }
    protected function getName(ComponentAttributeBag $attributes)
    {
        return $attributes->get('name', $attributes->get('id', ""));
    }
    protected function getLabel(ComponentAttributeBag $attributes)
    {
        return $attributes->get('label', "[DEFAULT LABEL]");
    }
    protected function getHideLabel(ComponentAttributeBag $attributes)
    {
        return $attributes->get('hide-label', false);
    }
    protected function getPlaceholder(ComponentAttributeBag $attributes)
    {
        $hideLabel = $this->getHideLabel($attributes);
        return $attributes->get('placeholder', $hideLabel ? $this->getLabel($attributes) : null);
    }
}
