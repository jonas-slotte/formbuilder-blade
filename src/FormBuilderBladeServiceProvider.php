<?php

namespace JonasSlotte\FormBuilderBlade;

use Illuminate\Support\ServiceProvider as Base;
use Illuminate\Support\Facades\Blade;

class FormBuilderBladeServiceProvider extends Base
{
    /**
     * The name of the configuration file/key
     *
     * @var string
     */
    protected $configName = "formbuilder-blade";

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Default package config
        $this->mergeConfigFrom(
            __DIR__ . "/../config/{$this->configName}.php",
            $this->configName
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Config file
        $this->publishes([
            __DIR__ . "/../config/{$this->configName}.php" => config_path("{$this->configName}.php")
        ]);

        //Load the config file with the theme definitions
        $config = $this->app["config"][$this->configName];
        $default = $config["theme"];
        $themes = $config["themes"];

        foreach ($themes as $theme => $data) {
            // The suffix it will be available at
            $suffixed = "formbuilder-$theme";

            // View publishing by theme
            $this->publishes([
                __DIR__ . "/../views/$theme" => resource_path("views/vendor/$suffixed"),
            ], "Theme: $theme");

            //View loading by name
            $this->loadViewsFrom(__DIR__ . "/../views/$theme", $suffixed);

            //Same components for all
            Blade::componentNamespace('JonasSlotte\\FormBuilderBlade\\View\\Components',   $suffixed);

            // Default theme is available without suffix
            // Dont publish this default, use aliasing.
            if ($theme ===  $default) {
                $this->loadViewsFrom(__DIR__ . "/../views/$theme", "formbuilder");
                Blade::componentNamespace('JonasSlotte\\FormBuilderBlade\\View\\Components', "formbuilder");
            }
        }
    }
}
